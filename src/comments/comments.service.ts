import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Comment } from "src/entities/comment.entity";
import { Repository } from "typeorm";
import { CreateCommentDto } from "./dto/create-comment.dto";

@Injectable()
export class CommentsService {
  public constructor(
    @InjectRepository(Comment)
    private readonly _commentsRepository: Repository<Comment>
  ) {}

  public async create(
    authorId: string,
    postId: string,
    { text }: CreateCommentDto
  ): Promise<Comment> {
    const commentObject = await this._commentsRepository.save({
      authorId,
      postId,
      text,
    });

    const comment = new Comment();
    Object.assign(comment, commentObject);

    return commentObject;
  }
}
