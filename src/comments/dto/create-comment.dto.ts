import {IsNotEmpty, IsString, MaxLength, MinLength} from "class-validator";

export class CreateCommentDto {
	@IsString()
	@IsNotEmpty()
	@MinLength(10)
	@MaxLength(500)
	public text: string;
}
