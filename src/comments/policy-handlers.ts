import { Comment } from "src/entities/comment.entity";
import { generateCrudPolicyHandlersForSubject } from "src/helpers/generateCrudPolicyHandlersForSubject";

export const commentsPolicyHandlers =
  generateCrudPolicyHandlersForSubject(Comment);
