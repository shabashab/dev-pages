import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Post } from "src/entities/post.entity";
import { User } from "src/entities/user.entity";
import { PostNotFoundException } from "src/exceptions";
import { Repository } from "typeorm";
import { CreatePostDto } from "./dto/create-post.dto";

@Injectable()
export class PostsService {
  public constructor(
    @InjectRepository(Post) private readonly _postsRepository: Repository<Post>
  ) {}

  public async findByAuthor(
    author: User,
    count?: number,
    offset?: number
  ): Promise<Post[]> {
    return await this._postsRepository.find({
      where: {
        author,
      },
      skip: offset,
      take: count
    });
  }

  public async findById(id: string): Promise<Post> {
    const post = await this._postsRepository.findOne(id);
    if (!post) throw new PostNotFoundException();
    return post;
  }

  public async findLast(count: number, offset: number): Promise<Post[]> {
    return await this._postsRepository.find({
      take: count,
      skip: offset,
    });
  }

  public async createPost(
    createPostDto: CreatePostDto,
    author: User
  ): Promise<Post> {
    const postObject = await this._postsRepository.save({
      title: createPostDto.title,
      content: createPostDto.content,
      author,
      createdAt: new Date(Date.now()),
    });

    const post = new Post();
    Object.assign(post, postObject);

    return post;
  }

  public async delete(post: Post): Promise<Post> {
    await this._postsRepository.delete({ id: post.id });
    return post;
  }
}
