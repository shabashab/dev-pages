import { Post } from "src/entities/post.entity";
import { generateCrudPolicyHandlersForSubject } from "src/helpers/generateCrudPolicyHandlersForSubject";

export const postsPolicyHandlers = generateCrudPolicyHandlersForSubject(Post);
