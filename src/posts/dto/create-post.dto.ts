import { IsString, IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class CreatePostDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  title: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(100)
  content: string;
}
