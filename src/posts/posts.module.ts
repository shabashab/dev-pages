import { Module } from "@nestjs/common";
import { PostsService } from "./posts.service";
import { PostsController } from "./posts.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Post } from "src/entities/post.entity";
import { CaslModule } from "src/casl/casl.module";
import { CommentsModule } from "src/comments/comments.module";

@Module({
  imports: [TypeOrmModule.forFeature([Post]), CaslModule, CommentsModule],
  providers: [PostsService],
  controllers: [PostsController],
})
export class PostsModule {}
