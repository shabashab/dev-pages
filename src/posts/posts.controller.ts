import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
} from "@nestjs/common";
import { CurrentUser } from "src/authentication/current-user.decorator";
import { RequireAuthentication } from "src/authentication/require-authentication.decorator";
import { CheckPolicies } from "src/casl/check-policies.decorator";
import { PoliciesCheckerService } from "src/casl/policies-checker.service";
import { CommentsService } from "src/comments/comments.service";
import { CreateCommentDto } from "src/comments/dto/create-comment.dto";
import { commentsPolicyHandlers } from "src/comments/policy-handlers";
import { Count, Offset } from "src/decorators";
import { User } from "src/entities/user.entity";
import { CreatePostDto } from "./dto/create-post.dto";
import { postsPolicyHandlers } from "./policy-handlers";
import { PostsService } from "./posts.service";

@Controller("posts")
export class PostsController {
  public constructor(
    private readonly _postsService: PostsService,
    private readonly _commentsService: CommentsService,
    private readonly _policiesCheckerService: PoliciesCheckerService
  ) {}

  @Get("own")
  @RequireAuthentication()
  public async getOwnPosts(
    @CurrentUser() user: User,
    @Count() count?: number,
    @Offset() offset?: number
  ) {
    return await this._postsService.findByAuthor(user, count, offset);
  }

  @Get("last")
  public async getLastPosts(
    @Count() count?: number,
    @Offset() offset?: number
  ) {
    return await this._postsService.findLast(count, offset);
  }

  @Post()
  @RequireAuthentication()
  @CheckPolicies(postsPolicyHandlers.create.forEvery)
  public async create(
    @CurrentUser() user: User,
    @Body() createPostDto: CreatePostDto
  ) {
    return await this._postsService.createPost(createPostDto, user);
  }

  @Delete(":id")
  @RequireAuthentication()
  @CheckPolicies(postsPolicyHandlers.delete.forEvery)
  public async delete(
    @CurrentUser() user: User,
    @Param("id", ParseUUIDPipe) id: string
  ) {
    const post = await this._postsService.findById(id);

    this._policiesCheckerService.checkPoliciesForUserOrThrowForbidden(user, [
      postsPolicyHandlers.delete.forSingle(post),
    ]);

    return await this._postsService.delete(post);
  }

  @Post(":id/comments")
  @RequireAuthentication()
  @CheckPolicies(commentsPolicyHandlers.create.forEvery)
  public async addComment(
    @CurrentUser() user: User,
    @Param("id", ParseUUIDPipe) id: string,
    @Body() createCommentDto: CreateCommentDto
  ) {
    return await this._commentsService.create(user.id, id, createCommentDto);
  }
}
