import { NotFoundException } from "@nestjs/common";

export class PostNotFoundException extends NotFoundException {
  constructor(message = "") {
    super("Post with specified parameters hasn't been found. " + message);
  }
}
