import { Injectable, ParseIntPipe } from "@nestjs/common";

@Injectable()
export class OptionalParseIntPipe extends ParseIntPipe {
  transform(value: string | undefined, metadata: any): any {
    if (value === undefined) return undefined;

    return super.transform(value, metadata);
  }
}
