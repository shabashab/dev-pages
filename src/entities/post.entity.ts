import { User } from "./user.entity";
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { Comment } from "./comment.entity";

@Entity()
export class Post {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column({
    nullable: false,
  })
  public title: string;

  @Column({
    nullable: false,
  })
  public content: string;

  @Column({
    nullable: false,
  })
  public createdAt: Date;

  @ManyToOne(() => User, (user) => user.posts)
  public author: User;

  @OneToMany(() => Comment, (comment) => comment.post, {
		eager: true
	})
  public comments: Comment[];
}
