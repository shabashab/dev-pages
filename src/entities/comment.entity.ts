import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Post } from "./post.entity";
import { User } from "./user.entity";

@Entity()
export class Comment {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column()
  public text: string;

  @Column({ nullable: false })
  public postId: string;

  @Column({ nullable: false })
  public authorId: string;

  @ManyToOne(() => Post, (post) => post.comments)
  public post: Post;

  @ManyToOne(() => User, (user) => user.comments)
  public author: User;
}
