import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Exclude } from "class-transformer";
import { RoleName } from "src/roles/role-name.enum";
import { Post } from "./post.entity";
import { Comment } from "./comment.entity";

@Entity()
export class User {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column({
    unique: true,
  })
  public username: string;

  @Column({
    nullable: false,
  })
  @Exclude()
  public passwordHash: string;

  @Column({
    type: "enum",
    enum: RoleName,
    nullable: false,
    default: RoleName.User,
  })
	@Exclude()
  public role: RoleName;

  @OneToMany(() => Post, (post) => post.author)
  public posts: Post[];

  @OneToMany(() => Comment, (comment) => comment.author)
	@Exclude()
  public comments: Comment[];
}
