import { Query } from "@nestjs/common";
import { OptionalParseIntPipe } from "src/pipes/optional-parse-int.pipe";

export const Count = () => Query("count", OptionalParseIntPipe);
