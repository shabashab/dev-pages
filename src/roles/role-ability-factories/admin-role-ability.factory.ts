import { Action } from "src/casl/action.enum";
import { Post } from "src/entities/post.entity";
import { User } from "src/entities/user.entity";
import {
  AppAbilityBuilder,
  RoleAbilityFactory,
} from "../role-ability.factory.interface";

export class AdminRoleAbilityFactory extends RoleAbilityFactory {
  configureAbilityBuilderForUser(
    _user: User,
    builder: AppAbilityBuilder
  ): AppAbilityBuilder {
    builder.can(Action.Manage, User);
    builder.can(Action.Manage, Post);

    return builder;
  }
}
