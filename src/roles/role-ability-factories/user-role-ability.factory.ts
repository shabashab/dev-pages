import { Action } from "src/casl/action.enum";
import { Post } from "src/entities/post.entity";
import { User } from "src/entities/user.entity";
import {
  AppAbilityBuilder,
  RoleAbilityFactory,
} from "../role-ability.factory.interface";

export class UserRoleAbilityFactory extends RoleAbilityFactory {
  configureAbilityBuilderForUser(
    user: User,
    builder: AppAbilityBuilder
  ): AppAbilityBuilder {
    builder.can([Action.Read, Action.Update], User, {
      id: user.id,
    });
    builder.cannot([Action.Create, Action.Delete, Action.ReadAll], User);

    builder.can([Action.ReadAll, Action.Read, Action.Create, Action.Comment], Post);
    builder.can([Action.Update, Action.Delete], Post, {
      author: { id: user.id },
    });

    return builder;
  }
}
