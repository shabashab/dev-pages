import { User } from "src/entities/user.entity";
import { Comment } from "src/entities/comment.entity";
import { Ability, InferSubjects } from "@casl/ability";
import { Action } from "./action.enum";
import { Post } from "src/entities/post.entity";

export type Subjects = InferSubjects<typeof User | typeof Post | typeof Comment> | "all";
export type AppAbility = Ability<[Action, Subjects]>;
